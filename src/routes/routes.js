import React from "react";
import { Route, Switch, Redirect } from "react-router-dom";
import Home from "../pages/HomePage/HomePage";
import AboutUs from "../pages/AboutUsPage/AboutUsPage";
import Archive from "../pages/ArchivePage/ArchivePage";
import LebPlay from "../pages/LebPlayPage/LebPlayPage";
import LebSpeak from "../pages/LebSpeekPage/LebSpeakPage";
import Contacts from "../pages/ContactsPage/ContactsPage";

function Routes() {
  return (
    <Switch>
      <Route path="/" exact component={Home} />
      <Route path="/about" exact component={AboutUs} />
      <Route path="/archive" exact component={Archive} />
      <Route path="/play" exact component={LebPlay} />
      <Route path="/speak" exact component={LebSpeak} />
      <Route path="/contacts" exact component={Contacts} />
      <Redirect to="/" />
    </Switch>
  );
}

export default Routes;
