import Routes from "./routes/routes";
import GlobalStyle from "./globalStyles/globalStyles";
import ScrollToTop from "./components/Util/ScrollToTop";
import { useSelector } from "react-redux";
import { BrowserRouter as Router } from "react-router-dom";
import { Footer, Navbar } from "./components/index";

function App() {
  const lightTheme = useSelector((state) => state.theme.lightTheme);
  return (
    <Router>
      <Navbar />
      <GlobalStyle lightTheme={lightTheme} />
      <ScrollToTop />

      <Routes />

      <Footer />
    </Router>
  );
}

export default App;
