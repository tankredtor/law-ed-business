import styled, { createGlobalStyle, keyframes } from "styled-components";
import {
  colorVariables,
  boxShadow,
  fontVariables,
  breakPoints,
} from "./variables";
import { Link } from "react-router-dom";

const GlobalStyle = createGlobalStyle`
@import url('https://fonts.googleapis.com/css2?family=Roboto&display=swap');
* {
    box-sizing: border-box;
  margin: 0;
  padding: 0;
  font-family: 'Roboto', sans-serif;
  
}
body{
  transition: all 0.6s ease-out;
  background: ${({ lightTheme }) =>
    lightTheme ? colorVariables.primaryWhite : colorVariables.primaryDark}
}
`;

export const Container = styled.div`
  z-index: 1;
  width: 100%;
  max-width: 1300px;
  margin-right: auto;
  margin-left: auto;
  padding-right: 50px;
  padding-left: 50px;
  display: grid;

  @media screen and (max-width: 991px) {
    padding-right: 30px;
    padding-left: 30px;
  }
`;

export const LinkRouter = styled(Link)`
  text-decoration: none;
  margin-bottom: 0.5rem;
  color: ${({ $lightTheme }) =>
    $lightTheme ? colorVariables.primaryPurple : colorVariables.primaryGreen};
  font-size: ${fontVariables.mediumDesctop};

  @media screen and (max-width: ${breakPoints.bpBiggest}) {
    font-size: ${fontVariables.mediumTab};
  }
  @media screen and (max-width: ${breakPoints.bpMedium}) {
    font-size: ${fontVariables.mediumPhone};
  }

  &:hover {
    transition: 0.3s ease-out;
    text-decoration: none;
    color: ${({ $lightTheme }) =>
      $lightTheme ? colorVariables.primaryPurple : colorVariables.primaryGreen};
  }
`;

export const Button = styled.button`
  transition: all 0.6s ease-out;
  border-radius: 4px;
  background: ${({ $lightTheme }) =>
    $lightTheme ? colorVariables.primaryPurple : colorVariables.primaryGreen};
  white-space: nowrap;
  padding: ${({ big }) => (big ? "12px 64px" : "10px 20px")};
  color: ${({ $lightTheme }) =>
    $lightTheme ? colorVariables.primaryWhite : colorVariables.primaryDark};
  max-width: 10rem;
  font-size: ${({ fontBig }) => (fontBig ? "20px" : "16px")};
  outline: none;
  font-weight: 700;
  border: none;

  cursor: pointer;

  &:hover {
    transition: all 0.6s ease-out;
    background: ${({ $lightTheme }) =>
      $lightTheme ? colorVariables.lightPurple : colorVariables.lightGreen};
  }

  @media screen and (max-width: 960px) {
    width: 100%;
  }
`;
export const ALink = styled.a`
  text-decoration: none;
  display: grid;
  justify-self: center;
  &:hover {
    text-decoration: none;
  }
`;
export const ActiveButton = styled(Button)`
  display: grid;
  width: auto;
  padding: 1rem;
  justify-self: center;
  justify-content: center;
  align-content: center;

  text-transform: uppercase;
  font-weight: 600;
  color: ${colorVariables.primaryDark};
  background-color: #fff;
  border: none;
  border-radius: 45px;
  box-shadow: 0px 8px 10px rgba(0, 0, 0, 0.1);
  transition: all 0.3s ease 0s !important;
  cursor: pointer;
  outline: none;
  font-size: ${fontVariables.smallDesctop};
  @media screen and (max-width: ${breakPoints.bpBiggest}) {
    font-size: ${fontVariables.mediumTab};
  }
  @media screen and (max-width: ${breakPoints.bpMedium}) {
    font-size: ${fontVariables.mediumPhone};
  }
  &,
  &:link,
  &:visited {
    text-decoration: none;
    text-align: center;
    border-radius: 10px;
    transition: all 0.2s;
    position: relative;
    cursor: pointer;
  }
  &:hover {
    background-color: ${({ lightTheme }) =>
      lightTheme ? colorVariables.primaryPurple : colorVariables.primaryGreen};
    box-shadow: 0px 15px 20px
      ${({ lightTheme }) =>
        lightTheme ? colorVariables.lightPurple : colorVariables.lightGreen};
    color: ${({ lightTheme }) =>
      lightTheme ? colorVariables.primaryWhite : colorVariables.primaryDark};
    transform: translateY(-7px);
  }
`;

const flashAnimationGreen = keyframes`
0%{
  box-shadow: 5px 5px 20px ${colorVariables.dimGreen},-5px -5px 20px  ${colorVariables.dimGreen};}
  
  50%{
  box-shadow: 5px 5px 20px ${colorVariables.lightGreen},-5px -5px 20px ${colorVariables.lightGreen};
  }
  100%{
  box-shadow: 5px 5px 20px ${colorVariables.dimGreen},-5px -5px 20px ${colorVariables.dimGreen};
  }
`;

const flashAnimationPurple = keyframes`
0%{
  box-shadow: 5px 5px 20px ${colorVariables.dimPurple},-5px -5px 20px  ${colorVariables.dimPurple};}
  
  50%{
  box-shadow: 5px 5px 20px ${colorVariables.lightPurple},-5px -5px 20px ${colorVariables.lightPurple};
  }
  100%{
  box-shadow: 5px 5px 20px ${colorVariables.dimPurple},-5px -5px 20px ${colorVariables.dimPurple};
  }
`;

export const FlashButton = styled.button`
  display: grid;
  width: auto;
  padding: 1rem;
  justify-self: center;
  justify-content: center;
  align-content: center;
  outline: none;
  text-transform: uppercase;
  font-weight: 600;
  border: none;
  padding: 1rem;
  border-radius: 4rem;
  transition: 0.3s;
  display: grid;
  justify-self: center;
  justify-content: center;
  align-content: center;
  cursor: pointer;
  background-color: ${colorVariables.dimWhite};
  animation: ${({ lightTheme }) =>
      lightTheme ? flashAnimationPurple : flashAnimationGreen}
    1.5s infinite;
  transition: 0.5s;
  font-size: ${fontVariables.smallDesctop};
  @media screen and (max-width: ${breakPoints.bpBiggest}) {
    font-size: ${fontVariables.mediumTab};
  }
  @media screen and (max-width: ${breakPoints.bpMedium}) {
    font-size: ${fontVariables.mediumPhone};
  }

  &:hover {
    transform: translateY(-5px);
    border-radius: 15px;
    background-color: #c3bacc;
    transition: 0.5s;
  }
`;

export default GlobalStyle;
