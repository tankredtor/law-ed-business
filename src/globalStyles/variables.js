export const colorVariables = {
  primaryDark: "#00171F",
  primaryGreen: "#61E786",
  primaryNude: "#C3A29E",
  primaryPurple: "#9381FF",
  primaryWhite: "#FFF4E9",
  lightDark: "#00394d",
  lightGreen: "#90eeab",
  lightNude: "#cfb3b0",
  lightPurple: "#a899ff",
  lightWhite: "#ffffff",
  dimDark: "#00131a",
  dimGreen: "#1ec84e",
  dimNude: "#9e6761",
  dimPurple: "#5233ff",
  dimWhite: "#f7edde",
};

export const fontVariables = {
  biggestDesctop: "5rem",
  biggestTab: "4rem",
  biggestPhone: "2.3rem",

  bigDesctop: "2rem",
  bigTab: "1.8rem",
  bigPhone: "1.6rem",

  mediumDesctop: "1.7rem",
  mediumTab: "1.2rem",
  mediumPhone: "1rem",

  smallDesctop: "1.2rem",
  smallTab: "0.8rem",
  smallPhone: "0.7rem",
};

export const breakPoints = {
  bpBiggest: "1200px",
  bpBig: "1000px",
  bpMedium: "800px",
  bpSmall: "600px",
  bpSmallest: "400px",
};

export const boxShadow = " 4px -2px 31px 0px rgba(34, 60, 80, 0.2)";
