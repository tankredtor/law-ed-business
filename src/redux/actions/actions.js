import { CHANGE_THEME } from "../types/types";

export function changeTheme() {
  return { type: CHANGE_THEME };
}
