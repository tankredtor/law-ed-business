import { CHANGE_THEME } from "../types/types";

const initialState = {
  lightTheme: true,
};

export const themeReducer = (state = initialState, action) => {
  switch (action.type) {
    case CHANGE_THEME:
      return { ...state, lightTheme: !state.lightTheme };
    default:
      return state;
  }
};
