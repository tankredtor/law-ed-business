export { default as PharmacySvg } from "./PharmacySvg";
export { default as IpSvg } from "./IpSvg";
export { default as ItSvg } from "./ItSvg";
export { default as AntitrustSvg } from "./AntitustSvg";
export { default as MapSvg } from "./MapSvg";
export { default as EveryStudentSvg } from "./EveryStudentSvg";
