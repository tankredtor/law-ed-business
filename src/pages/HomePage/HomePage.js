import React, { useState } from "react";
import { Carousel } from "../../components/index";
import img1 from "../../images/abstract.jpg";
function HomePage() {
  const [index, setIndex] = useState(0);

  const handleSelect = (selectedIndex, e) => {
    setIndex(selectedIndex);
  };

  return <Carousel></Carousel>;
}
export default HomePage;
