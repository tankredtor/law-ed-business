import React from "react";
import {
  Cards,
  Header,
  Schedule,
  Participate,
  Profit,
  Prize,
  Appeal,
} from "../../components/index";
import {
  headerObj,
  cardsObj,
  scheduleObj,
  participateObj,
  profitObj,
  prizeObj,
  appealObj,
} from "./Data";

function LebPlayPage() {
  return (
    <>
      <Header {...headerObj} />
      <Cards {...cardsObj} />
      <Schedule {...scheduleObj} />
      <Participate {...participateObj} />
      <Profit {...profitObj} />
      <Prize {...prizeObj} />
      <Appeal {...appealObj} />
    </>
  );
}

export default LebPlayPage;
