import styled from "styled-components";
import {
  colorVariables,
  fontVariables,
  breakPoints,
} from "../../globalStyles/variables";
import img from "../../images/abstractHome.jpg";

export const ScheduleBackground = styled.div`
  z-index: 1;
  width: 100%;
  max-width: 1300px;
  padding-top: 5rem;
  margin-right: auto;
  margin-left: auto;
  grid-template-rows: 0.3fr min-content min-content;
  row-gap: 1rem;
`;
export const ScheduleComponent = styled.div`
  justify-self: center;
  z-index: 2;
  margin-top: 2rem;
  display: grid;
  grid-template-rows: min-content;
  grid-template-columns: 1fr;
`;

export const ScheduleHeading = styled.h3`
  justify-self: center;
  transition: all 0.6s ease-out;
  color: ${({ $lightTheme }) =>
    $lightTheme ? colorVariables.primaryDark : colorVariables.primaryWhite};
  font-size: ${fontVariables.bigDesctop};
  font-weight: 700;
  margin-bottom: 2rem;

  @media screen and (max-width: ${breakPoints.bpBiggest}) {
    font-size: ${fontVariables.bigTab};
  }
  @media screen and (max-width: ${breakPoints.bpMedium}) {
    font-size: ${fontVariables.bigPhone};
  }
`;

export const ScheduleItem = styled.div`
  display: grid;
  grid-template-columns: min-content 65vw;
  justify-self: center;
  width: max-content;
  grid-template-rows: minmax(3rem, max-content);
  font-size: ${fontVariables.smallDesctop};
  @media screen and (max-width: ${breakPoints.bpBiggest}) {
    font-size: ${fontVariables.smallTab};
  }
  @media screen and (max-width: ${breakPoints.bpsmall}) {
    font-size: ${fontVariables.smallPhone};
  }
`;
export const ScheduleDate = styled.div`
  color: ${({ $lightTheme }) =>
    $lightTheme ? colorVariables.primaryDark : colorVariables.primaryWhite};
  display: grid;
  white-space: normal;
  justify-self: start;
  align-self: center;
  padding: 1rem;
  margin-bottom: 1.2rem;
  text-align: center;
  line-height: 1.8rem;
`;
