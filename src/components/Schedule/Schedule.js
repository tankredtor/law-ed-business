import React, { useRef, useState } from "react";
import { useSelector } from "react-redux";
import { Container } from "../../globalStyles/globalStyles";
import Accordion from "../Accordion/Accordion";
import {
  ScheduleDate,
  ScheduleItem,
  ScheduleComponent,
  ScheduleHeading,
  ScheduleBackground,
} from "./Schedule.elements";

function Schedule({
  heading,
  accordion1,
  accordion2,
  accordion3,
  accordion4,
  accordion5,
  accordion6,
  accordion7,
  accordion8,
  date1,
  date2,
  date3,
  date4,
  date5,
  date6,
  date7,
  date8,
}) {
  const lightTheme = useSelector((state) => state.theme.lightTheme);
  return (
    <ScheduleBackground>
      <ScheduleComponent>
        <ScheduleHeading $lightTheme={lightTheme}> {heading}</ScheduleHeading>
        <ScheduleItem>
          <ScheduleDate $lightTheme={lightTheme}>{date1}</ScheduleDate>
          <Accordion {...accordion1} />
        </ScheduleItem>
        <ScheduleItem>
          <ScheduleDate $lightTheme={lightTheme}>{date2}</ScheduleDate>
          <Accordion {...accordion2} />
        </ScheduleItem>
        <ScheduleItem>
          <ScheduleDate $lightTheme={lightTheme}>{date3}</ScheduleDate>
          <Accordion {...accordion3} />
        </ScheduleItem>
        <ScheduleItem>
          <ScheduleDate $lightTheme={lightTheme}>{date4}</ScheduleDate>
          <Accordion {...accordion4} />
        </ScheduleItem>
        <ScheduleItem>
          <ScheduleDate $lightTheme={lightTheme}>{date5}</ScheduleDate>
          <Accordion {...accordion5} />
        </ScheduleItem>
        <ScheduleItem>
          <ScheduleDate $lightTheme={lightTheme}>{date6}</ScheduleDate>
          <Accordion {...accordion6} />
        </ScheduleItem>
        <ScheduleItem>
          <ScheduleDate $lightTheme={lightTheme}>{date7}</ScheduleDate>
          <Accordion {...accordion7} />
        </ScheduleItem>
        <ScheduleItem>
          <ScheduleDate $lightTheme={lightTheme}>{date8}</ScheduleDate>
          <Accordion {...accordion8} />
        </ScheduleItem>
      </ScheduleComponent>
    </ScheduleBackground>
  );
}

export default Schedule;
