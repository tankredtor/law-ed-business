import styled, { css } from "styled-components";
import { Link } from "react-router-dom";
import {
  colorVariables,
  fontVariables,
  breakPoints,
} from "../../globalStyles/variables";
import { IoIosArrowBack, IoIosArrowForward } from "react-icons/io";
import img1 from "../../images/abstractHome.jpg";
import photoGirl from "../../images/PhotoGirl.jpg";

export const CarouselComponent = styled.div`
  justify-content: center;
  display: flex;
  z-index: 1;
  width: 100%;
  max-width: 1300px;
  margin-right: auto;
  margin-left: auto;
  align-items: center;
  overflow: hidden;
  transition: all 0.6s ease;
  background-image: linear-gradient(
      to right bottom,
      ${({ $lightTheme }) =>
        $lightTheme
          ? colorVariables.primaryPurple
          : colorVariables.primaryGreen}8c,
      ${({ $lightTheme }) =>
        $lightTheme ? colorVariables.lightPurple : colorVariables.lightGreen}3c
    ),
    url(${img1});
  background-size: cover;
  font-size: ${fontVariables.biggestDesctop};
  font-weight: 700;

  @media screen and (max-width: ${breakPoints.bpBiggest}) {
    font-size: ${fontVariables.biggestTab};
  }
  @media screen and (max-width: ${breakPoints.bpMedium}) {
    font-size: ${fontVariables.biggestPhone};
  }
  @media screen and (max-width: 1300px) {
    background-image: none;
  }
`;

export const SlidesComponent = styled.div`
  background-size: cover;
  z-index: 1;
  width: 100%;
  height: 100%;
  max-width: 1300px;
  margin-right: auto;
  margin-left: auto;
  display: flex;
  ${(props) =>
    props.currentSlide &&
    css`
      transform: translateX(-${props.currentSlide * 100}%);
    `};
  transition: all 1.3s ease;
`;

export const SlideComponent = styled.div`
  background-size: cover;
  z-index: 1;
  width: 100%;
  height: 100%;
  max-width: 1300px;
  margin-right: auto;
  margin-left: auto;
  flex: 0 0 auto;
  opacity: ${(props) => (props.active ? 1 : 0)};
  transition-property: clip-path;
  clip-path: ${(props) =>
    props.active ? "circle(150% at 0 50%)" : "circle(0% at 0 50%)"};
  transition: all 2s ease;
  width: 100%;
`;

export const SlideImage = styled(Link)`
  background-size: 100% 100%;
  z-index: 1;
  display: grid;
  width: 100%;
  height: 100%;
  height: 50vh;
  max-width: 1300px;
  margin-right: auto;
  margin-left: auto;
  background-image: url(${img1});
  transition: all 1s ease;
  text-decoration: none;
`;

export const SlideHeading = styled.h1`
  justify-self: center;
  font-size: ${fontVariables.biggestDesctop};
  text-align: center;
  color: ${colorVariables.primaryDark};
  @media screen and (max-width: ${breakPoints.bpBiggest}) {
    font-size: ${fontVariables.biggestTab};
  }
  @media screen and (max-width: ${breakPoints.bpMedium}) {
    font-size: ${fontVariables.biggestPhone};
    letter-spacing: 0.1rem;
  }
`;

export const SlideSpan = styled.span`
  background: rgba(144, 238, 171, 0.25);
  box-shadow: 0 8px 32px 0 rgba(31, 38, 135, 0.37);
  backdrop-filter: blur(10px);
  -webkit-backdrop-filter: blur(10px);
  border-radius: 10px;
  border: 1px solid rgba(255, 255, 255, 0.18);
  font-size: ${fontVariables.smallDesctop};
  text-align: center;
  @media screen and (max-width: ${breakPoints.bpBiggest}) {
    font-size: ${fontVariables.smallTab};
  }
  @media screen and (max-width: ${breakPoints.bpMedium}) {
    font-size: ${fontVariables.smallPhone};
    letter-spacing: 0.1rem;
  }
`;

export const SlideButtonPrev = styled(IoIosArrowBack)`
  cursor: pointer;
  z-index: 20;
  height: 50vh;
  color: ${({ $lightTheme }) =>
    $lightTheme ? colorVariables.primaryWhite : colorVariables.primaryDark};

  @media screen and (max-width: 1300px) {
    position: absolute;
    left: 0;
    opacity: 0.5;
    font-size: ${fontVariables.biggestTab};
    color: ${({ $lightTheme }) =>
      $lightTheme ? colorVariables.primaryPurple : colorVariables.primaryGreen};
  }
`;

export const SlideButtonNext = styled(IoIosArrowForward)`
  cursor: pointer;
  z-index: 20;
  height: 50vh;
  transition: all 0.6s ease;
  color: ${({ $lightTheme }) =>
    $lightTheme ? colorVariables.primaryWhite : colorVariables.primaryDark};

  @media screen and (max-width: 1300px) {
    position: absolute;
    opacity: 0.5;
    right: 0;
    transition: all 0.6s ease;
    font-size: ${fontVariables.biggestTab};
    color: ${({ $lightTheme }) =>
      $lightTheme ? colorVariables.primaryPurple : colorVariables.primaryGreen};
  }
`;
