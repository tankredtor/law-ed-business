import React, { useState, useEffect } from "react";
import { useSelector } from "react-redux";
import { ALink } from "../../globalStyles/globalStyles";
import {
  CarouselComponent,
  SlideComponent,
  SlidesComponent,
  SlideImage,
  SlideButtonNext,
  SlideButtonPrev,
  SlideHeading,
  SlideSpan,
} from "./Carousel.elements";

function Carousel() {
  const lightTheme = useSelector((state) => state.theme.lightTheme);
  const [currentSlide, setCurrentSlide] = useState(0);

  const slides = [
    <SlideImage to="/play">
      {" "}
      <SlideHeading> Law Ed Business: PLAY </SlideHeading>
    </SlideImage>,
    <SlideImage to="/speak">
      {" "}
      <SlideHeading> Law Ed Business: Speak</SlideHeading>
    </SlideImage>,
    <SlideImage to="/archive">
      {" "}
      <SlideHeading> Медиа</SlideHeading>
    </SlideImage>,
  ];

  const activeSlide = slides.map((slide, index) => (
    <SlideComponent active={currentSlide === index} key={index}>
      {slide}
    </SlideComponent>
  ));

  useEffect(() => {
    const timer = setTimeout(() => {
      setCurrentSlide((currentSlide + 1) % activeSlide.length);
    }, 6000);
    return () => clearTimeout(timer);
  }, [currentSlide, activeSlide]);

  return (
    <CarouselComponent $lightTheme={lightTheme}>
      <SlideButtonPrev
        $lightTheme={lightTheme}
        onClick={() => {
          setCurrentSlide(
            (currentSlide - 1 + activeSlide.length) % activeSlide.length
          );
        }}
      ></SlideButtonPrev>
      <SlidesComponent currentSlide={currentSlide}>
        {activeSlide}
      </SlidesComponent>
      <SlideButtonNext
        $lightTheme={lightTheme}
        onClick={() => {
          setCurrentSlide((currentSlide + 1) % activeSlide.length);
        }}
      ></SlideButtonNext>
    </CarouselComponent>
  );
}

export default Carousel;
