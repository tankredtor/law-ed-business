import styled from "styled-components";
import {
  colorVariables,
  fontVariables,
  breakPoints,
  boxShadow,
} from "../../globalStyles/variables";
import { FlashButton, Container } from "../../globalStyles/globalStyles";

export const AppealComponent = styled(Container)`
  justify-content: center;
  padding-top: 2rem;
  row-gap: 3rem;
  padding-bottom: 2rem;
  ${Container}
`;

export const AppealHeading = styled.h3`
  justify-self: center;
  transition: all 0.6s ease-out;
  font-size: ${fontVariables.bigDesctop};
  font-weight: 700;
  text-align: center;
  color: ${({ lightTheme }) =>
    lightTheme ? colorVariables.primaryDark : colorVariables.primaryWhite};

  @media screen and (max-width: ${breakPoints.bpBiggest}) {
    font-size: ${fontVariables.bigTab};
  }
  @media screen and (max-width: ${breakPoints.bpMedium}) {
    font-size: ${fontVariables.bigPhone};
  }
`;

export const Button = styled(FlashButton)`
  display: grid;
  justify-self: center;
`;
