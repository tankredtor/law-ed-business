import React from "react";
import { useSelector } from "react-redux";
import { ALink } from "../../globalStyles/globalStyles";
import { Button, AppealComponent, AppealHeading } from "./Appeal.elemets";
function Appeal({ heading, button }) {
  const lightTheme = useSelector((state) => state.theme.lightTheme);
  return (
    <AppealComponent>
      <AppealHeading lightTheme={lightTheme}>{heading}</AppealHeading>
      <ALink target="_blank" href="https://ip-academy.ru/">
        <Button lightTheme={lightTheme}>{button}</Button>
      </ALink>
    </AppealComponent>
  );
}

export default Appeal;
