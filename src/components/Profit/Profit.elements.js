import styled from "styled-components";
import {
  colorVariables,
  fontVariables,
  breakPoints,
  boxShadow,
} from "../../globalStyles/variables";
import { IoMdDoneAll } from "react-icons/io";

export const ProfitComponent = styled.div`
  z-index: 1;
  width: 100%;
  max-width: 1300px;
  margin-right: auto;
  margin-left: auto;
  padding-right: 50px;
  padding-left: 50px;
  width: 100%;
  padding-top: 2rem;
  display: grid;
  row-gap: 2rem;
  clip-path: polygon(0% 8%, 100% 0%, 100% 100%, 0% 100%);
  padding-bottom: 2rem;
`;

export const ProfitHeading = styled.h3`
  justify-self: center;
  transition: all 0.6s ease-out;
  color: ${({ lightTheme }) =>
    lightTheme ? colorVariables.primaryDark : colorVariables.primaryWhite};
  font-size: ${fontVariables.bigDesctop};
  font-weight: 700;
  text-align: center;

  @media screen and (max-width: ${breakPoints.bpBiggest}) {
    font-size: ${fontVariables.bigTab};
  }
  @media screen and (max-width: ${breakPoints.bpMedium}) {
    font-size: ${fontVariables.bigPhone};
  }
`;

export const ProfitUl = styled.ul`
  display: grid;
  justify-self: center;
  gap: 2rem;
  transition: all 0.6s ease-out;
  grid-template-columns: repeat(2, 1fr);
  color: ${({ lightTheme }) =>
    lightTheme ? colorVariables.primaryDark : colorVariables.primaryWhite};
  font-size: ${fontVariables.smallDesctop};
  font-weight: 700;

  @media screen and (max-width: ${breakPoints.bpBiggest}) {
    font-size: ${fontVariables.smallTab};
  }
  @media screen and (max-width: ${breakPoints.bpMedium}) {
    font-size: ${fontVariables.smallPhone};
    margin-left: 0rem;
  }

  @media screen and (max-width: 660px) {
    grid-template-rows: repeat(2, 1fr);
    grid-template-columns: 1fr;
  }
`;

export const ProfitLi = styled.li`
  display: grid;
  justify-content: center;
  justify-self: center;
  gap: 0.5rem;
  grid-template-columns: min-content minmax(min-content, max-content);
`;

export const ProfitLiIcon = styled(IoMdDoneAll)`
  display: grid;
  gap: 1rem;
  font-size: 1.5rem;
`;

export const ProfitLiP = styled.p`
  display: grid;
`;
