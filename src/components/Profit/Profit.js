import React from "react";
import { useSelector } from "react-redux";
import {
  ProfitComponent,
  ProfitHeading,
  ProfitLi,
  ProfitLiIcon,
  ProfitLiP,
  ProfitUl,
} from "./Profit.elements";
function Profit({ heading, description }) {
  const lightTheme = useSelector((state) => state.theme.lightTheme);
  return (
    <ProfitComponent>
      <ProfitHeading lightTheme={lightTheme}>{heading}</ProfitHeading>
      <ProfitUl lightTheme={lightTheme}>
        {description.map((data, index) => (
          <ProfitLi key={index}>
            <ProfitLiIcon />
            <ProfitLiP>{data.text}</ProfitLiP>
          </ProfitLi>
        ))}
      </ProfitUl>
    </ProfitComponent>
  );
}

export default Profit;
