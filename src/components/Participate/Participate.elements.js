import styled from "styled-components";
import {
  colorVariables,
  fontVariables,
  breakPoints,
  boxShadow,
} from "../../globalStyles/variables";
import { MapSvg, EveryStudentSvg } from "../../images/SVG/Cards/index";
import img from "../../images/abstractHome.jpg";
export const ParticipateComponent = styled.div`
  z-index: 1;
  width: 100%;
  max-width: 1300px;
  margin-right: auto;
  margin-left: auto;
  padding-right: 50px;
  padding-left: 50px;
  padding-top: 5rem;
  display: grid;
  grid-template-rows: 0.3fr min-content min-content;
  row-gap: 2rem;
  transition: all 0.6s ease-out;
  background-image: linear-gradient(
      to right bottom,
      ${({ lightTheme }) =>
        lightTheme
          ? colorVariables.primaryPurple
          : colorVariables.primaryGreen}8c,
      ${({ lightTheme }) =>
        lightTheme ? colorVariables.lightPurple : colorVariables.lightGreen}3c
    ),
    url(${img});
  background-size: cover;
  clip-path: polygon(0% 8%, 100% 0%, 100% 100%, 0% 100%);
  padding-bottom: 2rem;
`;

export const PaticipateHeading = styled.h3`
  justify-self: center;
  transition: all 0.6s ease-out;
  color: ${({ lightTheme }) =>
    lightTheme ? colorVariables.primaryWhite : colorVariables.primaryDark};
  font-size: ${fontVariables.bigDesctop};
  font-weight: 700;

  @media screen and (max-width: ${breakPoints.bpBiggest}) {
    font-size: ${fontVariables.bigTab};
  }
  @media screen and (max-width: ${breakPoints.bpMedium}) {
    font-size: ${fontVariables.bigPhone};
  }
`;

export const MapSvgCards = styled(MapSvg)`
  display: grid;
  grid-row: 2/3;
`;
export const StudentSvgCards = styled(EveryStudentSvg)`
  display: grid;
  grid-row: 2/3;
`;

export const PaticipateListOfCards = styled.div`
  display: grid;
  gap: 1rem;
  grid-template-columns: repeat(2, 1fr);

  @media screen and (max-width: 660px) {
    grid-template-rows: repeat(2, 1fr);
    grid-template-columns: 1fr;
  }
`;

export const CardItem = styled.div`
  display: grid;
  row-gap: 1rem;
  padding: 1rem;
  padding-top: 2rem;
  padding-bottom: 2rem;
  max-width: 25rem;
  grid-template-columns: 1fr;
  grid-template-rows: min-content minmax(1fr, 25rem) min-content;
  justify-self: center;
  transition: all 0.6s ease-out;
  box-shadow: ${boxShadow};
  background: ${({ lightTheme }) =>
    lightTheme ? colorVariables.dimWhite : colorVariables.dimDark};
  border-radius: 1rem;

  font-size: ${fontVariables.mediumDesctop};

  @media screen and (max-width: ${breakPoints.bpBiggest}) {
    font-size: ${fontVariables.mediumTab};
  }
  @media screen and (max-width: ${breakPoints.bpMedium}) {
    font-size: ${fontVariables.mediumPhone};
  }
`;

export const CardHeading = styled.div`
  display: grid;
  grid-row: 1/2;
  font-weight: 700;
  justify-self: center;
  transition: all 0.6s ease-out;
  text-align: center;
  color: ${({ lightTheme }) =>
    lightTheme ? colorVariables.primaryDark : colorVariables.primaryWhite};

  font-size: ${fontVariables.mediumDesctop};

  @media screen and (max-width: ${breakPoints.bpBiggest}) {
    font-size: ${fontVariables.mediumTab};
  }
  @media screen and (max-width: ${breakPoints.bpMedium}) {
    font-size: ${fontVariables.mediumPhone};
  }
`;

export const CardSpan = styled.div`
  display: grid;
  grid-row: 3/4;
  justify-self: center;
  align-self: start;
  transition: all 0.6s ease-out;
  color: ${({ lightTheme }) =>
    lightTheme ? colorVariables.primaryDark : colorVariables.primaryWhite};
`;
