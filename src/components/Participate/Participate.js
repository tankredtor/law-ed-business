import React from "react";
import { useSelector } from "react-redux";
import {
  ParticipateComponent,
  PaticipateHeading,
  PaticipateListOfCards,
  CardItem,
  CardSpan,
  CardHeading,
  MapSvgCards,
  StudentSvgCards,
} from "./Participate.elements";
import { ActiveButton, ALink } from "../../globalStyles/globalStyles";
function Participate({
  heading,
  card1heading,
  card1description,
  card2heading,
  card2description,
}) {
  const lightTheme = useSelector((state) => state.theme.lightTheme);
  return (
    <ParticipateComponent lightTheme={lightTheme}>
      <PaticipateHeading>{heading}</PaticipateHeading>
      <PaticipateListOfCards>
        <CardItem lightTheme={lightTheme}>
          <CardHeading lightTheme={lightTheme}>{card1heading}</CardHeading>
          <StudentSvgCards lightTheme={lightTheme} />
          <CardSpan lightTheme={lightTheme}>{card1description}</CardSpan>
        </CardItem>
        <CardItem lightTheme={lightTheme}>
          <CardHeading lightTheme={lightTheme}>{card2heading}</CardHeading>
          <MapSvgCards lightTheme={lightTheme} />
          <CardSpan lightTheme={lightTheme}>{card2description}</CardSpan>
        </CardItem>
      </PaticipateListOfCards>
      <ALink target="_blank" href="https://ip-academy.ru/">
        <ActiveButton lightTheme={lightTheme}>Участвовать</ActiveButton>
      </ALink>
    </ParticipateComponent>
  );
}

export default Participate;
