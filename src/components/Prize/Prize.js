import React from "react";
import { useSelector } from "react-redux";
import {
  PrizeHeading,
  PrizeComponent,
  PrizeLi,
  PrizetLiIconMoney,
  PrizetLiIconWork,
  PrizetLiP,
  PrizetUl,
} from "./Prize.elements";

function Prize({ heading, work, money }) {
  const lightTheme = useSelector((state) => state.theme.lightTheme);
  return (
    <PrizeComponent lightTheme={lightTheme}>
      <PrizeHeading lightTheme={lightTheme}>{heading}</PrizeHeading>
      <PrizetUl lightTheme={lightTheme}>
        <PrizeLi lightTheme={lightTheme}>
          <PrizetLiIconMoney />
          <PrizetLiP>{money}</PrizetLiP>
        </PrizeLi>
        <PrizeLi lightTheme={lightTheme}>
          <PrizetLiIconWork />
          <PrizetLiP>{work}</PrizetLiP>
        </PrizeLi>
      </PrizetUl>
    </PrizeComponent>
  );
}

export default Prize;
