import styled from "styled-components";
import {
  colorVariables,
  fontVariables,
  breakPoints,
  boxShadow,
} from "../../globalStyles/variables";
import img from "../../images/abstractHome.jpg";
import { FaMoneyBillWave } from "react-icons/fa";
import { MdWork } from "react-icons/md";

export const PrizeComponent = styled.div`
  z-index: 1;
  margin-right: auto;
  margin-left: auto;
  padding-right: 50px;
  padding-left: 50px;
  padding-top: 2rem;
  padding-bottom: 5rem;

  width: 100%;
  max-width: 1300px;
  display: grid;
  row-gap: 2rem;
  background-image: linear-gradient(
      to right bottom,
      ${({ lightTheme }) =>
        lightTheme
          ? colorVariables.primaryPurple
          : colorVariables.primaryGreen}8c,
      ${({ lightTheme }) =>
        lightTheme ? colorVariables.lightPurple : colorVariables.lightGreen}3c
    ),
    url(${img});
  background-size: cover;
  clip-path: polygon(0% 0%, 100% 0%, 100% 90%, 0% 100%);
`;

export const PrizeHeading = styled.h3`
  justify-self: center;
  transition: all 0.6s ease-out;
  font-size: ${fontVariables.bigDesctop};
  font-weight: 700;
  text-align: center;
  color: ${({ lightTheme }) =>
    lightTheme ? colorVariables.primaryWhite : colorVariables.primaryDark};

  @media screen and (max-width: ${breakPoints.bpBiggest}) {
    font-size: ${fontVariables.bigTab};
  }
  @media screen and (max-width: ${breakPoints.bpMedium}) {
    font-size: ${fontVariables.bigPhone};
  }
`;
export const PrizetUl = styled.ul`
  display: grid;
  justify-self: center;
  gap: 2rem;
  transition: all 0.6s ease-out;
  color: ${({ lightTheme }) =>
    lightTheme ? colorVariables.primaryDark : colorVariables.primaryWhite};
  font-size: ${fontVariables.smallDesctop};
  font-weight: 700;

  @media screen and (max-width: ${breakPoints.bpBiggest}) {
    font-size: ${fontVariables.smallTab};
  }
  @media screen and (max-width: ${breakPoints.bpMedium}) {
    font-size: ${fontVariables.smallPhone};
    margin-left: 0rem;
  }

  @media screen and (max-width: 660px) {
    grid-template-rows: repeat(2, 1fr);
    grid-template-columns: 1fr;
  }
`;

export const PrizeLi = styled.li`
  display: grid;
  justify-content: center;
  justify-self: center;
  gap: 0.5rem;
  box-shadow: ${boxShadow};
  border-radius: 1rem;
  padding: 1rem;
  max-width: 20rem;
  font-size: ${fontVariables.mediumDesctop};
  background: ${({ lightTheme }) =>
    lightTheme ? colorVariables.dimWhite : colorVariables.dimDark};
  @media screen and (max-width: ${breakPoints.bpBiggest}) {
    font-size: ${fontVariables.mediumTab};
  }
  @media screen and (max-width: ${breakPoints.bpMedium}) {
    font-size: ${fontVariables.mediumPhone};
  }
`;

export const PrizetLiIconMoney = styled(FaMoneyBillWave)`
  display: grid;
  gap: 1rem;
  font-size: 10rem;
  justify-self: center;
  color: ${colorVariables.primaryGreen};
  @media screen and (max-width: ${breakPoints.bpBig}) {
    font-size: 15rem;
  }
  @media screen and (max-width: ${breakPoints.bpSmall}) {
    font-size: 12rem;
  }
  @media screen and (max-width: ${breakPoints.bpSmallest}) {
    font-size: 8rem;
  }
`;

export const PrizetLiIconWork = styled(MdWork)`
  display: grid;
  gap: 1rem;
  font-size: 10rem;
  justify-self: center;
  color: ${colorVariables.primaryNude};
  @media screen and (max-width: ${breakPoints.bpBig}) {
    font-size: 15rem;
  }
  @media screen and (max-width: ${breakPoints.bpSmall}) {
    font-size: 12rem;
  }

  @media screen and (max-width: ${breakPoints.bpSmallest}) {
    font-size: 8rem;
  }
`;

export const PrizetLiP = styled.p`
  display: grid;
  text-align: center;
`;
