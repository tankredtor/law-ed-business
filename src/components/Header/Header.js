import React from "react";
import { useSelector } from "react-redux";
import {
  HeaderOfPage,
  HeadingBig,
  HeadingBiggest,
  SpanMedium,
} from "./Header.elements";

function HeaderComponent({ headingBiggest, headingBig, spanMedium }) {
  const lightTheme = useSelector((state) => state.theme.lightTheme);
  return (
    <HeaderOfPage lightTheme={lightTheme}>
      <HeadingBiggest lightTheme={lightTheme}>{headingBiggest}</HeadingBiggest>
      <HeadingBig>{headingBig}</HeadingBig>
      <SpanMedium>{spanMedium}</SpanMedium>
    </HeaderOfPage>
  );
}

export default HeaderComponent;
