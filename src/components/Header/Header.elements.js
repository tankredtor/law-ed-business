import styled from "styled-components";
import {
  colorVariables,
  fontVariables,
  breakPoints,
} from "../../globalStyles/variables";
import img from "../../images/abstractHome.jpg";

export const HeaderOfPage = styled.header`
  z-index: 1;
  width: 100%;
  max-width: 1300px;
  margin-right: auto;
  margin-left: auto;
  padding-right: 50px;
  padding-left: 50px;
  height: 550px;
  width: 100%;
  display: grid;
  grid-template-rows: 0.3fr min-content min-content;
  row-gap: 1rem;
  @media screen and (max-width: ${breakPoints.bpBiggest}) {
    grid-column: 1/14;
    height: 480px;
  }
  @media screen and (max-width: ${breakPoints.bpBig}) {
    height: 440px;
  }
  @media screen and (max-width: ${breakPoints.bpMedium}) {
    padding-right: 30px;
    padding-left: 30px;
    height: 300px;
  }
  @media screen and (max-width: ${breakPoints.bpSmall}) {
    height: 450px;
  }
  @media screen and (max-width: ${breakPoints.bpSmallest}) {
    height: 520px;
  }
  background-image: linear-gradient(
      to right bottom,
      ${({ lightTheme }) =>
        lightTheme
          ? colorVariables.primaryPurple
          : colorVariables.primaryGreen}8c,
      ${({ lightTheme }) =>
        lightTheme ? colorVariables.lightPurple : colorVariables.lightGreen}3c
    ),
    url(${img});
  background-size: cover;
  clip-path: polygon(0% 0%, 100% 0%, 100% 88%, 0% 100%);
`;

export const HeadingBiggest = styled.h1`
  font-size: ${fontVariables.biggestDesctop};
  justify-self: center;
  margin-top: 4rem;
  letter-spacing: 0.5rem;
  font-weight: 900;
  transition: all 0.6s ease-out;
  color: ${({ lightTheme }) =>
    lightTheme ? colorVariables.primaryWhite : colorVariables.primaryDark};
  text-align: center;
  @media screen and (max-width: ${breakPoints.bpBiggest}) {
    font-size: ${fontVariables.biggestTab};
  }
  @media screen and (max-width: ${breakPoints.bpMedium}) {
    font-size: ${fontVariables.biggestPhone};
    letter-spacing: 0.1rem;
  }
`;

export const HeadingBig = styled.h2`
  font-size: ${fontVariables.bigDesctop};
  justify-self: center;
  text-transform: uppercase;
  text-align: center;
  color: ${colorVariables.primaryDark};
  @media screen and (max-width: ${breakPoints.bpBiggest}) {
    font-size: ${fontVariables.bigTab};
  }
  @media screen and (max-width: ${breakPoints.bpMedium}) {
    font-size: ${fontVariables.bigPhone};
  }
`;

export const SpanMedium = styled.span`
  font-size: ${fontVariables.mediumDesctop};
  text-align: center;
  color: ${colorVariables.primaryDark};
  @media screen and (max-width: ${breakPoints.bpBiggest}) {
    font-size: ${fontVariables.mediumTab};
  }
  @media screen and (max-width: ${breakPoints.bpMedium}) {
    font-size: ${fontVariables.mediumPhone};
  }
`;
