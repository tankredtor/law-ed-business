import React from "react";
import { Container } from "../../globalStyles/globalStyles";
import {
  ListOfCards,
  CardItem,
  CardSpan,
  CardsComponent,
  CardsHeading,
} from "./CardsList.elements";
import {
  IpSvg,
  ItSvg,
  AntitrustSvg,
  PharmacySvg,
} from "../../images/SVG/Cards/index";
import { useSelector } from "react-redux";
function CardsList({ card1, card2, card3, card4, heading }) {
  const lightTheme = useSelector((state) => state.theme.lightTheme);

  return (
    <Container>
      <CardsComponent>
        <CardsHeading lightTheme={lightTheme}>{heading}</CardsHeading>
        <ListOfCards>
          <CardItem lightTheme={lightTheme}>
            <CardSpan lightTheme={lightTheme}>{card4}</CardSpan>
            <PharmacySvg lightTheme={lightTheme} />
          </CardItem>
          <CardItem lightTheme={lightTheme}>
            <CardSpan lightTheme={lightTheme}>{card1}</CardSpan>
            <IpSvg lightTheme={lightTheme} />
          </CardItem>
          <CardItem lightTheme={lightTheme}>
            {" "}
            <CardSpan lightTheme={lightTheme}>{card2}</CardSpan>
            <ItSvg lightTheme={lightTheme} />
          </CardItem>
          <CardItem lightTheme={lightTheme}>
            {" "}
            <CardSpan lightTheme={lightTheme}>{card3}</CardSpan>
            <AntitrustSvg lightTheme={lightTheme} />
          </CardItem>
        </ListOfCards>
      </CardsComponent>
    </Container>
  );
}

export default CardsList;
