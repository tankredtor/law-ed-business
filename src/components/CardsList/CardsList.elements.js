import styled from "styled-components";
import {
  colorVariables,
  fontVariables,
  breakPoints,
  boxShadow,
} from "../../globalStyles/variables";

export const CardsComponent = styled.div`
  display: grid;
  grid-template-rows: repeat(2, min-content);
  row-gap: 2rem;
  margin-top: 1rem;
  margin-bottom: 2rem;
`;

export const CardsHeading = styled.h3`
  justify-self: center;
  transition: all 0.6s ease-out;
  color: ${({ lightTheme }) =>
    lightTheme ? colorVariables.primaryDark : colorVariables.primaryWhite};
  font-size: ${fontVariables.bigDesctop};
  font-weight: 700;

  @media screen and (max-width: ${breakPoints.bpBiggest}) {
    font-size: ${fontVariables.bigTab};
  }
  @media screen and (max-width: ${breakPoints.bpMedium}) {
    font-size: ${fontVariables.bigPhone};
  }
`;

export const ListOfCards = styled.div`
  display: grid;
  gap: 1rem;
  grid-template-columns: repeat(4, 1fr);

  @media screen and (max-width: 1300px) {
    grid-template-rows: repeat(2, 1fr);
    grid-template-columns: repeat(2, 1fr);
  }

  @media screen and (max-width: 660px) {
    grid-template-rows: repeat(4, 1fr);
    grid-template-columns: 1fr;
  }
`;

export const CardItem = styled.div`
  display: grid;
  padding: 1rem;
  grid-template-columns: 1fr;
  grid-template-rows: min-content 1fr;
  justify-self: center;
  transition: all 0.6s ease-out;
  background: ${({ lightTheme }) =>
    lightTheme ? colorVariables.dimWhite : colorVariables.dimDark};
  border-radius: 1rem;
  box-shadow: ${boxShadow};
`;

export const CardSpan = styled.div`
  display: grid;
  grid-row: 2/3;
  justify-self: center;
  transition: all 0.6s ease-out;
  color: ${({ lightTheme }) =>
    lightTheme ? colorVariables.primaryDark : colorVariables.primaryWhite};

  font-size: ${fontVariables.mediumDesctop};

  @media screen and (max-width: ${breakPoints.bpBiggest}) {
    font-size: ${fontVariables.mediumTab};
  }
  @media screen and (max-width: ${breakPoints.bpMedium}) {
    font-size: ${fontVariables.mediumPhone};
  }
`;
