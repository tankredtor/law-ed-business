import React, { useState } from "react";
import { useSelector } from "react-redux";
import {
  AccordionTitle,
  AccordionTitleSpan,
  AccordionTitleArrow,
  AccordionContentSpan,
  AccordionContent,
  AccordionComponent,
} from "./Accordion.elements";
function Accordion({ title, description }) {
  const lightTheme = useSelector((state) => state.theme.lightTheme);
  const [open, setOpen] = useState(false);
  const openHandler = (e) => {
    e.preventDefault();
    setOpen((value) => !value);
  };
  return (
    <AccordionComponent>
      <AccordionTitle
        open={open}
        $lightTheme={lightTheme}
        onClick={openHandler}
      >
        <AccordionTitleSpan $lightTheme={lightTheme}>
          {title}
        </AccordionTitleSpan>
        <AccordionTitleArrow $lightTheme={lightTheme} open={open} size="30" />
      </AccordionTitle>
      <AccordionContent $lightTheme={lightTheme} open={open}>
        <AccordionContentSpan $lightTheme={lightTheme} open={open}>
          {description}
        </AccordionContentSpan>
      </AccordionContent>
    </AccordionComponent>
  );
}

export default Accordion;
