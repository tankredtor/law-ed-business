import styled from "styled-components";
import { IoIosArrowDown } from "react-icons/io";
import {
  colorVariables,
  fontVariables,
  breakPoints,
  boxShadow,
} from "../../globalStyles/variables";

export const AccordionComponent = styled.div`
  display: grid;
  justify-self: start;
  padding: 1rem;
  grid-template-columns: 1fr;
  align-self: start;
  width: 100%;
  margin-bottom: 1.3rem;
`;

export const AccordionTitle = styled.div`
  display: grid;
  grid-template-columns: 1fr min-content;
  padding: 1.5rem;
  width: available;
  transition: background 0.6s ease-out;
  background: ${({ $lightTheme }) =>
    $lightTheme ? colorVariables.dimWhite : colorVariables.dimDark};
  z-index: 4;
  cursor: pointer;
  border-radius: ${({ open }) => (open ? "1rem 1rem 0rem 0rem" : "1rem")};
  box-shadow: ${boxShadow};
`;

export const AccordionTitleSpan = styled.span`
  transition: all 0.6s ease-out;

  color: ${({ $lightTheme }) =>
    $lightTheme ? colorVariables.primaryDark : colorVariables.primaryWhite};
  align-self: center;
  padding-left: 1rem;
`;

export const AccordionTitleArrow = styled(IoIosArrowDown)`
  color: ${({ $lightTheme }) =>
    $lightTheme ? colorVariables.primaryPurple : colorVariables.primaryGreen};
  align-self: center;
  transform: ${({ open }) => (open ? "rotate(180deg)" : "rotate(0deg)")};
  transition: all 0.6s cubic-bezier(0.08, 1.09, 0.32, 1.275);
`;

export const AccordionContent = styled.div`
  display: grid;
  background-color: transparent;
  border-radius: 1rem;
  font-size: 14px;
  text-align: center;
  position: relative;
  z-index: 3;
  text-align: left;
  transition: all 200ms cubic-bezier(0.6, -0.28, 0.735, 0.045);
  color: ${({ $lightTheme }) =>
    $lightTheme ? colorVariables.primaryDark : colorVariables.primaryWhite};
  box-shadow: ${boxShadow};
`;

export const AccordionContentSpan = styled.span`
  padding: 1rem;
  transition: all 0.2s ease-out;
  display: ${({ open }) => (open ? "grid" : "none")};
  background: ${({ $lightTheme }) =>
    $lightTheme ? colorVariables.dimWhite : colorVariables.dimDark};
  visibility: ${({ open }) => (open ? "visible" : "hidden")};
  opacity: ${({ open }) => (open ? 1 : 0)};
  border-radius: 0rem 0rem 1rem 1rem;
  font-size: ${fontVariables.smallDesctop};
  @media screen and (max-width: ${breakPoints.bpBiggest}) {
    font-size: ${fontVariables.smallTab};
  }
  @media screen and (max-width: ${breakPoints.bpsmall}) {
    font-size: ${fontVariables.smallPhone};
  }
`;
