import { useSelector } from "react-redux";
import { FaFacebook, FaInstagram, FaYoutube } from "react-icons/fa";
import { Icon24LogoVk } from "@vkontakte/icons";
import {
  FooterContainer,
  FooterSubscription,
  FooterSubHeading,
  FooterLinksContainer,
  FooterLinksWrapper,
  FooterLinkItems,
  FooterLink,
  SocialMedia,
  SocialMediaWrap,
  SocialLogo,
  SocialIcon,
  WebsiteRights,
  SocialIcons,
  SocialIconLink,
} from "./Footer.elements";

function Footer() {
  const lightTheme = useSelector((state) => state.theme.lightTheme);
  const spanCurrentYear = () => {
    return `Law Ed Business © ${new Date().getFullYear()}`;
  };
  return (
    <FooterContainer $lightTheme={lightTheme}>
      <FooterSubscription $lightTheme={lightTheme}>
        <FooterSubHeading>Присоединяйтесь к нашим проектам</FooterSubHeading>
      </FooterSubscription>
      <FooterLinksContainer>
        <FooterLinksWrapper>
          <FooterLinkItems>
            <FooterLink $lightTheme={lightTheme} to="/play">
              LEB:Play
            </FooterLink>
          </FooterLinkItems>
          <FooterLinkItems>
            <FooterLink $lightTheme={lightTheme} to="/">
              LEB:Speak
            </FooterLink>
          </FooterLinkItems>
        </FooterLinksWrapper>
        <FooterLinksWrapper>
          <FooterLinkItems>
            <FooterLink $lightTheme={lightTheme} to="/archive">
              Медиа
            </FooterLink>
          </FooterLinkItems>
          <FooterLinkItems>
            <FooterLink $lightTheme={lightTheme} to="/contacts">
              Контакты
            </FooterLink>
          </FooterLinkItems>
        </FooterLinksWrapper>
      </FooterLinksContainer>
      <SocialMedia>
        <SocialMediaWrap>
          <SocialLogo $lightTheme={lightTheme} to="/">
            <SocialIcon />
            Law Ed Business
          </SocialLogo>
          <WebsiteRights>{spanCurrentYear()}</WebsiteRights>
          <SocialIcons $lightTheme={lightTheme}>
            <SocialIconLink
              $lightTheme={lightTheme}
              href="https://www.facebook.com/pro.education.msal/"
              target="_blank"
              aria-label="Facebook"
            >
              <FaFacebook />
            </SocialIconLink>
            <SocialIconLink
              $lightTheme={lightTheme}
              href="https://www.instagram.com/pro.education_/"
              target="_blank"
              aria-label="Instagram"
            >
              <FaInstagram />
            </SocialIconLink>
            <SocialIconLink
              $lightTheme={lightTheme}
              href="https://vk.com/pro.education_msal/"
              target="_blank"
              aria-label="vk"
            >
              <Icon24LogoVk width={28} height={28} />
            </SocialIconLink>
            <SocialIconLink
              $lightTheme={lightTheme}
              href={"https://www.youtube.com/channel/UCsOyVbyOqcMsQBg445zpViQ/"}
              rel="noopener noreferrer"
              target="_blank"
              aria-label="Youtube"
            >
              <FaYoutube />
            </SocialIconLink>
          </SocialIcons>
        </SocialMediaWrap>
      </SocialMedia>
    </FooterContainer>
  );
}

export default Footer;
