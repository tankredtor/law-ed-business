import styled from "styled-components";
import { SiMinetest } from "react-icons/si";
import { NavLink } from "react-router-dom";
import { Container, Button } from "../../globalStyles/globalStyles";
import { colorVariables, boxShadow } from "../../globalStyles/variables";
import { BsMoon, BsSun } from "react-icons/bs";

export const Nav = styled.nav`
  transition: all 0.6s ease-out;
  background: ${({ $lightTheme }) =>
    $lightTheme ? colorVariables.primaryWhite : colorVariables.primaryDark};
  box-shadow: 0px 18px 36px 6px rgba(34, 60, 80, 0.15);
  height: 80px;
  display: flex;
  justify-content: center;
  align-items: center;
  font-size: 1.2rem;
  position: sticky;
  top: 0;
  z-index: 999;
`;

export const NavbarContainer = styled(Container)`
  display: flex;
  justify-content: space-between;
  height: 80px;

  ${Container}
`;

export const NavLogo = styled(NavLink)`
  justify-self: flex-start;
  cursor: pointer;
  text-decoration: none;
  font-size: 2rem;
  display: flex;
  align-items: center;
  color: ${({ $lightTheme }) =>
    $lightTheme ? colorVariables.primaryPurple : colorVariables.primaryGreen};
  &:hover {
    color: ${({ $lightTheme }) =>
      $lightTheme ? colorVariables.primaryPurple : colorVariables.primaryGreen};
    text-decoration: none;
  }
`;

export const NavIcon = styled(SiMinetest)`
  margin-right: 0.5rem;
`;

export const MobileIcon = styled.div`
  display: none;

  @media screen and (max-width: 960px) {
    display: block;
    position: absolute;
    top: 0;
    right: 0;
    transform: translate(-100%, 60%);
    font-size: 1.8rem;
    cursor: pointer;
    color: ${({ $lightTheme }) =>
      $lightTheme ? colorVariables.primaryPurple : colorVariables.primaryGreen};
  }
`;

export const NavMenu = styled.ul`
  display: flex;
  align-items: center;
  list-style: none;
  text-align: center;

  @media screen and (max-width: 960px) {
    display: flex;
    flex-direction: column;
    width: 100%;
    height: 100vh;
    position: absolute;
    top: 80px;
    left: ${({ click }) => (click ? 0 : "-100%")};
    opacity: 1;
    transition: all 0.5s ease;
    background: ${({ $lightTheme }) =>
      $lightTheme ? colorVariables.primaryWhite : colorVariables.primaryDark};
  }
`;

export const NavItem = styled.li`
  height: 80px;

  @media screen and (max-width: 960px) {
    width: 100%;
  }
`;

export const NavItemBtn = styled.li`
  @media screen and (max-width: 960px) {
    display: flex;
    justify-content: center;
    align-items: center;
    width: 100%;
    height: 120px;
  }
`;
const activeClassName = "nav-item-active";
export const NavLinks = styled(NavLink).attrs({ activeClassName })`
  color: ${({ $lightTheme }) =>
    $lightTheme ? colorVariables.primaryDark : colorVariables.primaryWhite};
  display: flex;
  align-items: center;
  text-decoration: none;
  padding: 0rem 1rem;
  height: 100%;
  transition: all 0.5s ease;
  border-bottom: 2px solid transparent;
  &:hover {
    color: ${({ $lightTheme }) =>
      $lightTheme ? colorVariables.primaryPurple : colorVariables.primaryGreen};
    text-decoration: none;
  }
  &.${activeClassName} {
    color: ${({ $lightTheme }) =>
      $lightTheme ? colorVariables.primaryPurple : colorVariables.primaryGreen};
    border-bottom: 2px solid
      ${({ $lightTheme }) =>
        $lightTheme
          ? colorVariables.primaryPurple
          : colorVariables.primaryGreen};
    @media screen and (max-width: 960px) {
      border-bottom: none;
    }
  }

  @media screen and (max-width: 960px) {
    text-align: center;
    padding: 2rem;
    width: 100%;
    display: table;
    border-bottom: none;
  }
`;

export const NavBtnLink = styled.button`
  display: flex;
  justify-content: space-between;
  align-items: center;
  text-decoration: none;
  border: none;
  outline: none;
  transition: all 0.6s ease;
  width: 4rem;
  height: 2rem;
  border-radius: 2rem;
  cursor: pointer;
  box-shadow: ${boxShadow};
  overflow: hidden;
  font-size: 2rem;
  background: ${({ $lightTheme }) =>
    $lightTheme ? colorVariables.dimWhite : colorVariables.lightDark};
`;

export const ThemeToggleIconMoon = styled(BsMoon)`
  color: ${colorVariables.primaryGreen};
  transition: all 0.6s ease;
  transform: ${({ $lightTheme }) =>
    $lightTheme ? "translateY(-100px)" : "translateY(0)"};
`;

export const ThemeToggleIconSun = styled(BsSun)`
  color: orange;
  transition: all 0.6s ease;
  transform: ${({ $lightTheme }) =>
    $lightTheme ? "translateY(0)" : "translateY(100px)"};
`;
