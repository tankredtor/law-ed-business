import React, { useState } from "react";
import { FaBars, FaTimes } from "react-icons/fa";
import { useDispatch, useSelector } from "react-redux";
import { changeTheme } from "../../redux/actions/actions";
import {
  Nav,
  NavbarContainer,
  NavIcon,
  NavItem,
  NavLinks,
  NavLogo,
  NavMenu,
  MobileIcon,
  NavBtnLink,
  ThemeToggleIconMoon,
  ThemeToggleIconSun,
} from "./Navbar.elements";

function Navbar() {
  const dispatch = useDispatch();
  const lightTheme = useSelector((state) => state.theme.lightTheme);

  const themeChangeHandler = (e) => {
    e.preventDefault();
    dispatch(changeTheme());
  };

  const [click, setClick] = useState(false);

  const handleClick = () => setClick(!click);
  const closeMobileMenu = () => setClick(false);

  return (
    <Nav $lightTheme={lightTheme}>
      <NavbarContainer>
        <NavLogo to="/" $lightTheme={lightTheme} onClick={closeMobileMenu}>
          <NavIcon />
          LEB
        </NavLogo>
        <MobileIcon $lightTheme={lightTheme} onClick={handleClick}>
          {click ? <FaTimes /> : <FaBars />}
        </MobileIcon>
        <NavMenu onClick={handleClick} $lightTheme={lightTheme} click={click}>
          <NavItem>
            <NavLinks
              $lightTheme={lightTheme}
              exact
              to="/"
              onClick={closeMobileMenu}
            >
              Главная
            </NavLinks>
          </NavItem>
          <NavItem>
            <NavLinks
              $lightTheme={lightTheme}
              to="/play"
              onClick={closeMobileMenu}
            >
              LEB:Play
            </NavLinks>
          </NavItem>
          <NavItem>
            <NavLinks
              $lightTheme={lightTheme}
              to="/speak"
              onClick={closeMobileMenu}
            >
              LEB:Speak
            </NavLinks>
          </NavItem>
          <NavItem>
            <NavLinks
              $lightTheme={lightTheme}
              to="/archive"
              onClick={closeMobileMenu}
            >
              Медиа
            </NavLinks>
          </NavItem>
          <NavItem>
            <NavLinks
              $lightTheme={lightTheme}
              to="/contacts"
              onClick={closeMobileMenu}
            >
              Контакты
            </NavLinks>
          </NavItem>

          <NavBtnLink $lightTheme={lightTheme} onClick={themeChangeHandler}>
            <ThemeToggleIconSun $lightTheme={lightTheme} />
            <ThemeToggleIconMoon $lightTheme={lightTheme} />
          </NavBtnLink>
        </NavMenu>
      </NavbarContainer>
    </Nav>
  );
}

export default Navbar;
